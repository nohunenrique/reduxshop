@extends('layouts.app')

@section('content')
<!--================Login Box Area =================-->
<section class="login_box_area section_gap">
    <div class="container">
        <div class="row">
            <div class="col-lg-6">
                <div class="login_box_img">
                    <img class="img-fluid" src="img/login.jpg" alt="">
                    <div class="hover">
                        <h4>¿Eres un nuevo usuario?</h4>
                        <p>Descubre todo lo que te podemos ofreser, para ayudarte a ti y al ambiente</p>
                        <a class="primary-btn" href="{{ route ('register')}}">Registrate</a>
                    </div>
                </div>
            </div>
            {{-- <div class="row justify-content-center"> --}}
                <div class="col-lg-6">
                    {{-- <div class="card"> --}}
                        {{-- <div class="card-header">{{ __('Login') }}</div> --}}

                        <div class="login_form_inner">
                            <h3>Iniciar Sesión</h3>
                            <form method="POST" action="{{ route('login') }}" class="row login_form">
                                @csrf


                                    {{-- <label for="email" class="col-md-4 col-form-label text-md-right">{{ __('E-Mail Address') }}</label> --}}

                                    <div class="col-md-12 form-group">
                                        <input id="email" type="email" class="form-control @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}" required autocomplete="email" placeholder="Email" onfocus="this.placeholder = ''" onblur="this.placeholder = 'email'">

                                        @error('email')
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $message }}</strong>
                                            </span>
                                        @enderror
                                    </div>





                                    <div class="col-md-12 form-group">
                                        <input id="password" type="password" class="form-control @error('password') is-invalid @enderror" name="password" required autocomplete="current-password" placeholder="Contraseña" onfocus="this.placeholder = ''" onblur="this.placeholder = 'Password'">

                                        @error('password')
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $message }}</strong>
                                            </span>
                                        @enderror
                                    </div>



                                    <div class="col-md-12 form-group">
                                        <div class="creat_account">
                                            <input class="" type="checkbox" name="remember" id="remember" {{ old('remember') ? 'checked' : '' }}>

                                            <label class="" for="remember">
                                                {{ __('Recordarme') }}
                                            </label>
                                        </div>
                                    </div>



                                    <div class="col-md-12 form-group">
                                        <button type="submit" class="primary-btn">
                                            {{ __('Iniciar Sesión') }}
                                        </button>

                                        @if (Route::has('password.request'))
                                            <a class="btn btn-link" href="{{ route('password.request') }}">
                                                {{ __('¿Olvidate tu contraseña?') }}
                                            </a>
                                        @endif
                                    </div>

                            </form>
                        </div>

                </div>
            </div>

    </div>
</section>
<!--================End Login Box Area =================-->









@endsection
