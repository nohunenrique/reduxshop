@extends('layouts.app')
<br>
<br>
<br>
<br>
<br>
@section('content')
<section class="login_box_area section_gap">
    <div class="container">
        <div class="row justify-content-center">

            <div class="col-md-8">
                    <div class="login_form_inner">
                        <h3 class="center">Restablecer contraseña</h3>
                        @if (session('status'))
                            <div class="alert alert-success" role="alert">
                                {{ session('status') }}
                            </div>
                        @endif


                        <form method="POST" action="{{ route('password.email') }}"  class="row login_form">
                            @csrf



                                <div class="col-md-12 form-group">
                                    <input id="email" type="email" class="form-control @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}" required autocomplete="email" placeholder="email" onfocus="this.placeholder = ''" onblur="this.placeholder = 'email'">

                                    @error('email')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror
                                </div>



                                <div class="col-md-12 form-group">
                                    <button type="submit" class="primary-btn">
                                        {{ __('Enviar enlace') }}
                                    </button>
                                </div>

                        </form>
                    </div>

            </div>
        </div>
    </div>
</section>
@endsection
