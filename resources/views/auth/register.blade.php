@extends('layouts.app')

@section('content')
<section class="login_box_area section_gap">
    <div class="container">
        <div class="row justify-content-center">

            <div class="col-lg-6">
                <div class="login_box_img">
                    <img class="img-fluid" src="img/register.jpg" alt="" style="height: 500px">
                    <div class="hover">
                        <h4>¿Ya tienes una cuenta?</h4>
                        <p>No esperes más y disfruta con nostros</p>
                        <a class="primary-btn" href="{{ route ('login')}}">Iniciar Sesión</a>
                    </div>
                </div>
            </div>
             <div class="col-lg-6">


                <div class="login_form_inner">
                    <h3>Registrarse</h3>

                    <form method="POST" action="{{ route('register') }}" class="row login_form">
                        @csrf
                            <div class="col-md-12 form-group">
                                <input id="name" type="text" class="form-control @error('name') is-invalid @enderror" name="name" value="{{ old('name') }}" required autocomplete="name" placeholder="Usuario" onfocus="this.placeholder = ''" onblur="this.placeholder = 'Usuario'">

                                @error('name')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>





                            <div class="col-md-12 form-group">
                                <input id="email" type="email" class="form-control @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}" required autocomplete="email" placeholder="Email" onfocus="this.placeholder = ''" onblur="this.placeholder = 'email'">

                                @error('email')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>





                            <div class="col-md-12 form-group">
                                <input id="password" type="password" class="form-control @error('password') is-invalid @enderror" name="password" required autocomplete="new-password" placeholder="Password" onfocus="this.placeholder = ''" onblur="this.placeholder = 'Password'">

                                @error('password')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>





                            <div class="col-md-12 form-group">
                                <input id="password-confirm" type="password" class="form-control" name="password_confirmation" required autocomplete="new-password"
                                placeholder="Confirmar Password" onfocus="this.placeholder = ''" onblur="this.placeholder = 'Confirmar Password'">
                            </div>



                            <div class="col-md-12 form-group">
                                <button type="submit" class="primary-btn">
                                    {{ __('Register') }}
                                </button>
                            </div>

                    </form>
                </div>

        </div>

        </div>
    </div>
</section>
@endsection
