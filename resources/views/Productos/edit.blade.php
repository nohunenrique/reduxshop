@extends('layouts.app')

@section('content')
<div class="row">
    <div class="card">
        <form action="post"  action="{{ url('productos.edit'.$producto->id)}}">
            @csrf
            @method('PUT')

            <label for="Titulo">{{'Titulo'}}</label>
            <input type="text" name="Titulo" id="Titulo" value="{{$producto->titulo}}">
            <br>
            <label for="Descripcion">{{'Descripcion'}}</label>
            <input type="text" name="Descripcion" id="Descripcion" value="{{$producto->descripcion}}">
            <br>
            <label for="Precio">{{'Precio'}}</label>
            <input type="text" name="Precio" id="Precio" value="{{$producto->precio}}">
            <br>

            <label for="Foto">{{'Foto'}}</label>

            {{$producto->foto}}
            <br>
            <input type="file" name="Foto" id="Foto" value="{{$producto->foto}}">
            <br>
            <input type="submit" value="editar">
        </form>
    </div>
</div>
@endsection
