

@extends('layouts.app')

@section('content')

<br>
<br>
<br>
<br>
<br>
<div class="container-center">
    <div class="row justify-content-center">
        <div class="col-lg-6">
            <div class="card">
                <table class="table table-light">
                    <thead class="thead-light">
                        <tr>
                            <th> # </th>
                            <th> titulo </th>
                            <th> Descripcion </th>
                            <th> precio </th>
                            <th> acciones </th>
                            <th>Crear</th>
                            <th>Editar</th>
                            <th>Eliminar</th>

                        </tr>
                    </thead>
                    <tbody>
                        @foreach ($productos as $producto)
                            <tr>
                                <td>{{$loop->iteration}}</td>
                                <td>{{$producto->titulo}}</td>
                                <td>{{$producto->descripcion}}</td>
                                <td>{{$producto->precio}}</td>
                                <td>{{$producto->foto}}</td>
                                <td>
                                    <a href=" {{url('productos/create')}}  ">
                                        crear
                                    </a>
                                </td>

                                <td>
                                    <a href=" {{url('productos/edit')}}  ">
                                        editar
                                    </a>
                                </td>
                                <td>
                                    <form action="post"  action="{{ url('productos/'.$producto->id)}}">
                                        {{ csrf_field() }}
                                        {{ method_field('DELETE') }}
                                        <button type="submit" onclick="return confirm('¿borrar?')"> Borrar </button>

                                    </form>
                                </td>

                            </tr>
                        @endforeach
                        <tr>
                            <td></td>
                        </tr>
                    </tbody>
                </table>
            </div>
        </div>

    </div>
</div>
@endsection
