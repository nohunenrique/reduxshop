<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('index');
});

Route::get('/category', function () {
    return view('category');
});
Route::get('/blog', function () {
    return view('blog');
});
Route::get('/cart', function () {
    return view('cart');
});
Route::get('/confirmation', function () {
    return view('confirmation');
});
Route::get('/checkout', function () {
    return view('checkout');
});
Route::get('/single_product', function () {
    return view('single_product');
});
Route::get('/single_blog', function () {
    return view('single_blog');
});
Route::get('/single_blog2', function () {
    return view('single_blog2');
});
Route::get('/contact', function () {
    return view('contact');
});
Route::get('/login', function () {
    return view('/login');
});
Route::resource('productos', 'ProductController');

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

